//
// Created by Joachim on 08.05.2022.
//

#include "stack.h"
#include <malloc.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>

int
stackInit(IntStack *self) {
	if (!self) {
		return -1;
	}
	self->top = NULL;
	self->size = 0;
	return 0;
}

void
stackRelease(IntStack *self) {
	StackElement* tmp = self->top;
	while (tmp) {
		StackElement* tmp2 = tmp;
		tmp = tmp->previous;
		free(tmp2);
	}
	self->top = NULL;
	self->size = 0;
}

void
stackPush(IntStack *self, int i) {
	StackElement* tmp = calloc(1, sizeof(StackElement));
	tmp->value = i;
	if (self->top) { // Stack beinhaltet schon Elemente
		tmp->previous = self->top;	// altes oberes Elemente als vorheriges des neuen oberen Speichern
	}
	self->top = tmp;
	self->size++;
}

int
stackTop(const IntStack *self) {
	if (!self->top) {
		errno = ENODATA;
		perror("Stack ist leer");
		exit(-errno);
	}
	return self->top->value;
}

int
stackPop(IntStack *self) {
	if (!self->size) {
		errno = ENODATA;
		perror("Stack ist leer");
		exit(-errno);
	}
	int tmp = self->top->value;
	StackElement* tmp_p = self->top;
	self->top = tmp_p->previous; 	// oberstes Element auf das vorherige setzen. Falls letztes Element, dann ist previous = NULL
	free(tmp_p);
	self->size--;
	return tmp;
}

int
stackIsEmpty(const IntStack *self) {
	return (!self->size);
}

void
stackPrint(const IntStack *self) {

}