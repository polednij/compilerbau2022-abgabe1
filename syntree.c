//
// Created by Joachim on 08.05.2022.
//

#include "syntree.h"
#include <stdlib.h>
#include <stdio.h>

/*
 * Erläuterungen zum Code:
 * Die Nodes beinhalten ein Type-Feld, mittels welchem entschieden wird, ob es ein Literalknoten oder ein Listenknoten ist.
 * Der Wert des Nodes ist eine Union aus int bzw dem Pointer auf die Liste.
 *
 * Die ID des Nodes entspricht dem zugehörigen Pointer
 *
 * Das Syntree Objekt ist als Stack repräsentiert, da der Baum in dieser implementation nur wachsen kann.
 * Wenn jeweils ein neuer Node angelegt wird, wird der Pointer dazu im Stack gespeichert.
 * Das wird dazu benötigt, um auch nicht zusammenhänge Bäume beim Freigeben des Syntrees restlos zu löschen und keine Speicherlecks zu erzeugen.
 *
 */

/*
 * Prüfe ob der Pointer zu self gültig ist.
 * Setze dann die Felder auf die Initialen Werte
 */
int syntreeInit(Syntree *self) {
	if (!self) {
		return -1;
	}
	self->top = NULL;
	self->size = 0;
	return 0;
}

/*
 * Löse den Node Stack auf
 * Traversiere die Liste in Listenknoten und gebe diese Liste frei
 */
void
syntreeRelease(Syntree *self) {
	if (!self) {
		exit(-1);
	}
	SyntreeNodeStackNode* tmp = self->top;
	while (tmp) {
		SyntreeNodeStackNode* tmp2 = tmp;

		if (tmp->node && tmp->node->type_ == List) {
			SyntreeNodeList* list = tmp->node->list_->first;
			if (list->first == list->last) {
				free(list);
				list = NULL;
			}
			while (list) {
				SyntreeNodeList* tmp3 = list;
				list = list->next;
				free(tmp3);
			}
		}
		free(tmp->node);

		tmp = tmp->previous;
		free(tmp2);
	}
	self->top = NULL;
	self->size = 0;
}

/*
 * Füge einen neuen Node in den Nodestack ein
 */
void
stackPush(Syntree *self, SyntreeNode* i) {
	if (!self || !i) {
		exit(-1);
	}
	SyntreeNodeStackNode* tmp = calloc(1, sizeof(SyntreeNodeStackNode));
	tmp->node = i;
	if (self->top) { // Stack beinhaltet schon Elemente
		tmp->previous = self->top;	// altes oberes Elemente als vorheriges des neuen oberen Speichern
	}
	self->top = tmp;
	self->size++;
}

/*
 * Erzeuge einen neuen Literalknoten und füge ihn dem Node-Stack hinzu
 */
SyntreeNodeID syntreeNodeNumber(Syntree *self, int number) {
	if (!self) {
		exit(-1);
	}
	SyntreeNode* tmp = malloc(sizeof(SyntreeNode));
	tmp->type_ = Literal;
	tmp->value_ = number;
	stackPush(self, tmp);
	return tmp;
}

/*
 * Erzeuge einen neuen Listenknoten und initialisiere die Liste.
 * Füge dann den zu kapselnden Knoten ein.
 * Füge den Listenknoten der Node-Stack hinzu.
 */
SyntreeNodeID syntreeNodeTag(Syntree *self, SyntreeNodeID id) {
	if (!self || !id) {
		exit(-1);
	}
	SyntreeNode* tmp = malloc(sizeof(SyntreeNode));
	tmp->type_ = List;
	tmp->list_ = calloc(1, sizeof(SyntreeNodeList));
	tmp->list_->value = id;
	tmp->list_->first = tmp->list_->last = tmp->list_;
	stackPush(self, tmp);
	return tmp;
}

/*
 * Bilde ein Paar aus Knoten.
 * Erzeuge dafür einen neuen Listenknoten und füge beide Ausgangsknoten dieser Liste hinzu.
 */
SyntreeNodeID syntreeNodePair(Syntree *self, SyntreeNodeID id1, SyntreeNodeID id2) {
	if (!self || !id1 || !id2) {
		exit(-1);
	}
	SyntreeNode* tmp = syntreeNodeTag(self,id1);
	syntreeNodeAppend(self, tmp, id2);
	return tmp;
}

/*
 * Beim Anhängen an eine Liste muss kein neuer Knoten erzeugt werden.
 * Einzig ein Listenelement muss erzeugt werden und in die Liste eingefügt werden.
 */
SyntreeNodeID syntreeNodeAppend(Syntree *self, SyntreeNodeID list, SyntreeNodeID elem) {
	if (!self || !list || !elem) {
		exit(-1);
	}
	if (list->type_ != List)
		exit(-1);
	SyntreeNodeList* tmp = list->list_->last->next = calloc(1, sizeof(SyntreeNodeList));
	tmp->previous = list->list_->last;
	list->list_->last = tmp->last = tmp;
	tmp->value = elem;
	return list;
}

/*
 * Entspricht Append nur für den Anfang der Liste
 */
SyntreeNodeID syntreeNodePrepend(Syntree *self, SyntreeNodeID elem, SyntreeNodeID list) {
	if (!self || !list || !elem) {
		exit(-1);
	}
	if (list->type_ != List)
		exit(-1);
	SyntreeNodeList* tmp = list->list_->first->previous = calloc(1, sizeof(SyntreeNodeList));
	tmp->next = list->list_->first;
	list->list_->first = tmp->first = tmp;
	tmp->value = elem;
	return list;
}

/*
 * Ausgabe des Baumes ausgehend vom root-Knoten
 * Der Baum wird rekursiv traversiert. Listenknoten werden in einer Schleife abgearbeitet.
 */
void syntreePrint(const Syntree *self, SyntreeNodeID root) {
	if (!self || !root) {
		exit(-1);
	}
	static int level = 0;
	if (root->type_ == List) {
		for (int i = 0; i < level; i++) {
			printf("\t");
		}
		level++;
		printf("{\n");
		SyntreeNodeList* tmp = root->list_->first;
		while (tmp) {
			syntreePrint(self, tmp->value);
			tmp = tmp->next;
//			printf("%p", tmp->next);
		}
		level--;
		for (int i = 0; i < level; i++) {
			printf("\t");
		}
		printf("}\n");
	}
	else {
		for (int i = 0; i < level; i++) {
			printf("\t");
		}
		printf("(%d)\n", root->value_);
	}
}
